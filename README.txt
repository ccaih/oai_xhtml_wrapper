=================
OAI_XHTML_WRAPPER
=================

WSGI wrapper which can perform server-side transform to XHTML with a given stylesheet
or inject XSLT into OAI-PMH XML response before passing down-stream.

For usage, see ./examples subdirectory.

Customise the oai_index.html and oai2.xsl (or in fact, copy the stylesheet to a new one
as you can specify the name of a different .xsl stylesheet to use in the wrapper
configuration).



Configuration:
-------------- 

The wrapper takes a configuration dict as argument (see source).
Values for the following keys will be parsed and used.


result_format_override

	Valid values: "xml", "xhtml"   (OR blank, and no quotes)

	If blank, default behaviour is to return XML in all cases except when client
	gives a non-zero HTTP_ACCEPT header which does not contain an XML mimetype
	and is not the accept-all "*/*"


xslt_files_root

	Full local path to static resources (path containing 'oai_index.html', 'oai2.xsl' etc)


xslt_uri_root

	Web path to static resources directory e.g. "/static" (minus the quotes)


xslt_file
 
	Name of XSLT file to use (must live in static path configured as per above).
	If not given or not readable, default XSL is applied.


external_host

	The host URI (proto:host<:port>) for external consumption (if we're running behind a proxy, etc)
	This ensures we publish relevant URIs to consumers (e.g. output of verb=Identify query).

