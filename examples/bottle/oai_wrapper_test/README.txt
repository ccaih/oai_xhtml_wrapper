
File: app.py
	Contains core OAI application (a simple dummy app, in this case) AND the wrapper
	injection code using Bottle's mount() API.

Assumes "oai_xhtml_wrapper" package has been installed into your Python environment.

