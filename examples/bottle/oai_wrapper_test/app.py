import sys
from bottle import Bottle, request, response, route, run, static_file
from urllib import urlopen
from pprint import pformat


## ==========================================================================
## Import the OAI -> XHTML wrapper
from oai_xhtml_wrapper import oai_xhtml_wrapper
## ==========================================================================


## Dummy OAI XML feed from external source
OAI_PMH_URI = 'http://research-hub.griffith.edu.au/oai'
## IP address to serve on
SERVER_HOST = '127.0.0.1'
## Port to listen on
SERVER_PORT = 9090


root_app = Bottle()


### DEBUG ###
def print_environ_app(environ, start_response):
    """
    Dummy application which returns the REQUEST environment dict for debugging.
    """
    envstr = pformat(environ)
    response_headers = [('Content-Type', 'text/plain')]
    start_response('200 OK', response_headers)
    return envstr
### END DEBUG ###
 

## Simple OAI WSGI application which is being wrapped. 
def my_oai_application(environ, start_response):
    """
    Simple application which returns an OAI XML feed from remote.
    A "proxy" front-end, if you will.
    """
    ## extract target verb from environment
    query_string = environ.get('QUERY_STRING', 'verb=Identify')
    oai_xml_file = urlopen(OAI_PMH_URI + '?' + query_string)
    oai_xml = oai_xml_file.read()
    oai_xml_file.close()

    response_headers = [('Content-Type', 'text/xml')]
    start_response('200 OK', response_headers)
    return [oai_xml]


@root_app.route('/static/<filename>')
def server_static(filename):
    sys.stderr.write("DEBUG: Static file fetched = " + filename + "\n")
    return static_file(filename, root='static')


## ==========================================================================
## Plug in OAI wrapper. Listen on "/oai"
## There are more ways to do this, but mount is convenient because we specify
## the OAI endpoint URI we want.
##
root_app.mount('/oai', oai_xhtml_wrapper(my_oai_application, {
    'xslt_files_root':'static',
    'xslt_uri_root':'/static',
    'xslt_file':'oai2.xsl',
    ## blank override means we use default behaviour based on HTTP_ACCEPT
    ## supplied by client
    'result_format_override':'',
    ## The external host to return in OAI query results e.g. Identify query.
    ## So we don't return 'localhost:9090' or '127.0.0.1:8080' type host strings!
    'external_host':'http://research-hub.griffith.edu.au'
}))
## ==========================================================================

### DEBUG ###
root_app.mount('/environ', print_environ_app)
### END DEBUG ###

run(app=root_app, host=SERVER_HOST, port=SERVER_PORT, debug=True)

