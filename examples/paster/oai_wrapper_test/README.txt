
A simple Paster-based OAI server application that injects the OAI XHTML wrapper
as a Paster "filter" - WSGI middleware.

We need to add some configuration items to the .ini file to make this happen
(see the .ini file) and we add a "FilterFactory" instance to kick things off.

