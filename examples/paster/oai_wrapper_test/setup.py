from setuptools import setup, find_packages
import os

version = '0.1'

setup(name='myoaiapp',
      version=version,
      description="My Paster OAI App",
      long_description="""
         Very, very minimal example of a WSGI application and middleware.
      """,
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        ],
      keywords='',
      author='',
      author_email='',
      url='',
      license='',
      include_package_data=True,
      packages=['myoaiapp',],
      zip_safe=False,
      install_requires=[
          'setuptools','oai_xhtml_wrapper'
      ],
      entry_points="""
      [paste.app_factory] 
      main = myoaiapp:main
      
      [paste.filter_factory]
      main = myoaiapp:wrapper
      wrapper = myoaiapp:wrapper
      """,
      )
