import sys
from urllib import urlopen

## ==========================================================================
## Import OAI XHTML wrapper
from oai_xhtml_wrapper import *
## ==========================================================================


## Dummy OAI XML feed from external source
OAI_PMH_URI = 'http://research-hub.griffith.edu.au/oai'


## Original WSGI application which is being wrapped. 
def simple_app(environ, start_response):
    """
    Simple application which returns an OAI XML feed from remote.
    A "proxy" front-end, if you will.
    """
    query_string = environ.get('QUERY_STRING', 'verb=Identify')
    try:
        oai_xml_file = urlopen(OAI_PMH_URI + '?' + query_string)
        oai_xml = oai_xml_file.read()
        oai_xml_file.close()
    except Exception, e:
        start_response('500 Internal Server Error', [('Content-Type','text/plain')])
        return ['MyOAIApp Error: ' + str(e)]

    start_response('200 OK', [('Content-Type','text/xml')], sys.exc_info())
    return [oai_xml]


## ==========================================================================
## Inject OAI XHTML wrapper.
##
## This boilerplate code is needed to satisfy Paster's "FilterFactory"
## based instantiation of WSGI middleware.
##
## This entry point is configured in the "[filter:oai_wrapper]" section
## of the paster .ini file, and entry_points in setup.py.
##
def wrapper(global_config, **settings):
    def filter(app):
        return oai_xhtml_wrapper(app, settings)
    return filter
## ==========================================================================


def main(global_config, **settings):
    ## (Not passing any config to application)
    return simple_app

