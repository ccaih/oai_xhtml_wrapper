#!/usr/bin/env python

from setuptools import setup, find_packages

version = '0.2dev'

setup(
    name='oai_xhtml_wrapper',
    version=version,
    description="WSGI wrapper for OAI-PMH to XHTML output",
    long_description="""
        WSGI wrapper which can perform server-side transform to XHTML with a given stylesheet
        or inject XSLT into OAI-PMH XML response before passing down-stream.
    """,
    # Get more strings from
    # http://pypi.python.org/pypi?:action=list_classifiers
    classifiers=[
        "Programming Language :: Python",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Middleware",
    ],
    keywords='OAI,XSLT,XHTML,WSGI',
    author='Othmar Korn',
    author_email='othmar.korn AT gmail.com',
    maintainer="James Mills",
    maintainer_email="James Mills, j dot mills at griffith dot edu dot au",
    url='',
    license='',
    include_package_data=True,
    package_dir={"": "src"},
    packages=find_packages("src"),
    zip_safe=False,
    ## Also tested working in lxml 3.1beta1
    install_requires=[
        'lxml>=2.3.3'
    ],
    entry_points={
        "paste.filter_app_factory": [
            "oai_filter = oai_xhtml_wrapper.wsgi:oai_filter",
        ]
    }
)
