import os
import re
import sys
from StringIO import StringIO
from urllib import urlopen
from lxml import etree, html


def default_xslt_body():
    '''
    If no XSLT stylesheet given, use this embedded one to get up and running quickly.
    '''

    return """<?xml version="1.0" encoding="utf-8"?>
<!--

  XSL Transform to convert OAI 2.0 responses into XHTML

  By Christopher Gutteridge, University of Southampton

  v1.1

  Modified by O.Korn, eResearch Services, Griffith University
      * ListRecords and ListIdentifiers use metadataPrefix from request
        instead of assuming type 'oai_dc'
      * Outputs XML markup instead of erroring on unrecognised metadata
-->

<!-- 
  
Copyright (c) 2006 University of Southampton, UK. SO17 1BJ.

EPrints 3 is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

EPrints 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EPrints 3; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

-->

   
<!--
  
  All the elements really needed for EPrints are done but if
  you want to use this XSL for other OAI archive you may want
  to make some minor changes or additions.

  Not Done
    The 'about' section of 'record'
    The 'compession' part of 'identify'
    The optional attributes of 'resumptionToken'
    The optional 'setDescription' container of 'set'

  All the links just link to oai_dc versions of records.

-->
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:oai="http://www.openarchives.org/OAI/2.0/"
>

<xsl:output method="html"/>



<xsl:template name="style">
td.value {
	vertical-align: top;
	padding-left: 1em;
	padding: 3px;
}
td.key {
	background-color: #e0e0ff;
	padding: 3px;
	text-align: right;
	border: 1px solid #c0c0c0;
	white-space: nowrap;
	font-weight: bold;
	vertical-align: top;
}
.dcdata td.key {
	background-color: #ffffe0;
}
body { 
	margin: 1em 2em 1em 2em;
}
h1, h2, h3 {
	font-family: sans-serif;
	clear: left;
}
h1 {
	padding-bottom: 4px;
	margin-bottom: 0px;
}
h2 {
	margin-bottom: 0.5em;
}
h3 {
	margin-bottom: 0.3em;
	font-size: medium;
}
.link {
	border: 1px outset #88f;
	background-color: #c0c0ff;
	padding: 1px 4px 1px 4px;
	font-size: 80%;
	text-decoration: none;
	font-weight: bold;
	font-family: sans-serif;
	color: black;
}
.link:hover {
	color: red;
}
.link:active {
	color: red;
	border: 1px inset #88f;
	background-color: #a0a0df;
}
.oaiRecord, .oaiRecordTitle {
	background-color: #f0f0ff;
	border-style: solid;
	border-color: #d0d0d0;
}
h2.oaiRecordTitle {
	background-color: #e0e0ff;
	font-size: medium;
	font-weight: bold;
	padding: 10px;
	border-width: 2px 2px 0px 2px;
	margin: 0px;
}
.oaiRecord {
	margin-bottom: 3em;
	border-width: 2px;
	padding: 10px;
}

.results {
	margin-bottom: 1.5em;
}
ul.quicklinks {
	margin-top: 2px;
	padding: 4px;
	text-align: left;
	border-bottom: 2px solid #ccc;
	border-top: 2px solid #ccc;
	clear: left;
}
ul.quicklinks li {
	font-size: 80%;
	display: inline;
	list-stlye: none;
	font-family: sans-serif;
}
p.intro {
	font-size: 80%;
}
<xsl:call-template name='xmlstyle' />
</xsl:template>

<xsl:variable name='identifier' select="substring-before(concat(substring-after(/oai:OAI-PMH/oai:request,'identifier='),'&amp;'),'&amp;')" />

<xsl:template match="/">
<html>
  <head>
    <title>OAI 2.0 Request Results</title>
    <style><xsl:call-template name="style"/></style>
  </head>
  <body>
    <h1>OAI 2.0 Request Results</h1>
    <xsl:call-template name="quicklinks"/>
    <p class="intro">You are viewing an HTML version of the XML OAI response. To see the underlying XML use your web browsers view source option. More information about this XSLT is at the <a href="#moreinfo">bottom of the page</a>.</p>
    <xsl:apply-templates select="/oai:OAI-PMH" />
    <xsl:call-template name="quicklinks"/>
    <h3><a name="moreinfo">About the XSLT</a></h3>
    <small><p>An XSLT file has converted the <a href="http://www.openarchives.org">OAI-PMH 2.0</a> responses into XHTML which looks nice in a modern browser which supports XSLT whilst also enabling non-XML capable clients (e.g. web crawlers) to parse the OAI-PMH results.</p><p>The XSLT file is a modified version originally created by <a href="http://www.ecs.soton.ac.uk/people/cjg">Christopher Gutteridge</a> at the University of Southampton as part of the <a href="http://www.eprints.org/software/">GNU EPrints system</a>, and is freely redistributable under the <a href="http://www.gnu.org">GPL</a>.</p><p>(If you want to use the XSL file on your own OAI interface you may but due to the way XSLT works you must install the XSL file on the same server as the OAI script, you can't just link to this copy).</p><p>For more information or to download the XSL file please see the <a href="http://software.eprints.org/xslt.php">OAI to XHTML XSLT homepage</a>.</p></small>

  </body>
</html>
</xsl:template>

<xsl:template name="quicklinks">
    <ul class="quicklinks">
      <li><a href="?verb=Identify">Identify</a> | </li> 
      <li><a href="?verb=ListRecords&amp;metadataPrefix=oai_dc">ListRecords</a> | </li>
      <li><a href="?verb=ListSets">ListSets</a> | </li>
      <li><a href="?verb=ListMetadataFormats">ListMetadataFormats</a> | </li>
      <li><a href="?verb=ListIdentifiers&amp;metadataPrefix=oai_dc">ListIdentifiers</a></li>
    </ul>
</xsl:template>


<xsl:template match="/oai:OAI-PMH">
  <table class="values">
    <tr><td class="key">Datestamp of response</td>
    <td class="value"><xsl:value-of select="oai:responseDate"/></td></tr>
    <tr><td class="key">Request URL</td>
    <td class="value"><xsl:value-of select="oai:request"/></td></tr>
  </table>
<!--  verb: [<xsl:value-of select="oai:request/@verb" />]<br /> -->
  <xsl:choose>
    <xsl:when test="oai:error">
      <h2>OAI Error(s)</h2>
      <p>The request could not be completed due to the following error or errors.</p>
      <div class="results">
        <xsl:apply-templates select="oai:error"/>
      </div>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="metadataPrefix" select="oai:request/@metadataPrefix"/>
      <xsl:choose>
        <xsl:when test="$metadataPrefix != ''">
          <p>Request was of type <xsl:value-of select="oai:request/@verb"/> for metadata format <xsl:value-of select="$metadataPrefix"/>.</p>
        </xsl:when>
        <xsl:otherwise>
          <p>Request was of type <xsl:value-of select="oai:request/@verb"/>.</p>
        </xsl:otherwise>
      </xsl:choose>

      <div class="results">
        <xsl:apply-templates select="oai:Identify" />
        <xsl:apply-templates select="oai:GetRecord"/>
        <xsl:apply-templates select="oai:ListRecords"/>
        <xsl:apply-templates select="oai:ListSets"/>
        <xsl:apply-templates select="oai:ListMetadataFormats"/>
        <xsl:apply-templates select="oai:ListIdentifiers"/>
      </div>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ERROR -->

<xsl:template match="/oai:OAI-PMH/oai:error">
  <table class="values">
    <tr><td class="key">Error Code</td>
    <td class="value"><xsl:value-of select="@code"/></td></tr>
  </table>
  <p class="error"><xsl:value-of select="." /></p>
</xsl:template>

<!-- IDENTIFY -->

<xsl:template match="/oai:OAI-PMH/oai:Identify">
  <table class="values">
    <tr><td class="key">Repository Name</td>
    <td class="value"><xsl:value-of select="oai:repositoryName"/></td></tr>
    <tr><td class="key">Base URL</td>
    <td class="value"><xsl:value-of select="oai:baseURL"/></td></tr>
    <tr><td class="key">Protocol Version</td>
    <td class="value"><xsl:value-of select="oai:protocolVersion"/></td></tr>
    <tr><td class="key">Earliest Datestamp</td>
    <td class="value"><xsl:value-of select="oai:earliestDatestamp"/></td></tr>
    <tr><td class="key">Deleted Record Policy</td>
    <td class="value"><xsl:value-of select="oai:deletedRecord"/></td></tr>
    <tr><td class="key">Granularity</td>
    <td class="value"><xsl:value-of select="oai:granularity"/></td></tr>
    <xsl:apply-templates select="oai:adminEmail"/>
    <xsl:apply-templates select="oai:description"/>
  </table>
</xsl:template>

<xsl:template match="/oai:OAI-PMH/oai:Identify/oai:adminEmail">
    <tr><td class="key">Admin Email</td>
    <td class="value"><xsl:value-of select="."/></td></tr>
</xsl:template>

<!--
   Identify / Description ("as-is" output)
-->

<xsl:template match="oai:description">
    <tr><td class="key">Description</td>
    <td class="value">
    <xsl:apply-templates select="./*" mode='xmlMarkup' />
    </td></tr>
</xsl:template>


<!--
   Identify / OAI-Identifier
-->

<xsl:template match="id:oai-identifier" xmlns:id="http://www.openarchives.org/OAI/2.0/oai-identifier">
  <h2>OAI-Identifier</h2>
  <table class="values">
    <tr><td class="key">Scheme</td>
    <td class="value"><xsl:value-of select="id:scheme"/></td></tr>
    <tr><td class="key">Repository Identifier</td>
    <td class="value"><xsl:value-of select="id:repositoryIdentifier"/></td></tr>
    <tr><td class="key">Delimiter</td>
    <td class="value"><xsl:value-of select="id:delimiter"/></td></tr>
    <tr><td class="key">Sample OAI Identifier</td>
    <td class="value"><xsl:value-of select="id:sampleIdentifier"/></td></tr>
  </table>
</xsl:template>


<!--
   Identify / EPrints
-->

<xsl:template match="ep:eprints" xmlns:ep="http://www.openarchives.org/OAI/1.1/eprints">
  <h2>EPrints Description</h2>
  <xsl:if test="ep:content">
    <h3>Content</h3>
    <xsl:apply-templates select="ep:content"/>
  </xsl:if>
  <xsl:if test="ep:submissionPolicy">
    <h3>Submission Policy</h3>
    <xsl:apply-templates select="ep:submissionPolicy"/>
  </xsl:if>
  <h3>Metadata Policy</h3>
  <xsl:apply-templates select="ep:metadataPolicy"/>
  <h3>Data Policy</h3>
  <xsl:apply-templates select="ep:dataPolicy"/>
  <xsl:apply-templates select="ep:comment"/>
</xsl:template>

<xsl:template match="ep:content|ep:dataPolicy|ep:metadataPolicy|ep:submissionPolicy" xmlns:ep="http://www.openarchives.org/OAI/1.1/eprints">
  <xsl:if test="ep:text">
    <p><xsl:value-of select="ep:text" /></p>
  </xsl:if>
  <xsl:if test="ep:URL">
    <div><a href="{ep:URL}"><xsl:value-of select="ep:URL" /></a></div>
  </xsl:if>
</xsl:template>

<xsl:template match="ep:comment" xmlns:ep="http://www.openarchives.org/OAI/1.1/eprints">
  <h3>Comment</h3>
  <div><xsl:value-of select="."/></div>
</xsl:template>


<!--
   Identify / Friends
-->

<xsl:template match="fr:friends" xmlns:fr="http://www.openarchives.org/OAI/2.0/friends/">
  <h2>Friends</h2>
  <ul>
    <xsl:apply-templates select="fr:baseURL"/>
  </ul>
</xsl:template>

<xsl:template match="fr:baseURL" xmlns:fr="http://www.openarchives.org/OAI/2.0/friends/">
  <li><xsl:value-of select="."/> 
<xsl:text> </xsl:text>
<a class="link" href="{.}?verb=Identify">Identify</a></li>
</xsl:template>


<!--
   Identify / Branding
-->

<xsl:template match="br:branding" xmlns:br="http://www.openarchives.org/OAI/2.0/branding/">
  <h2>Branding</h2>
  <xsl:apply-templates select="br:collectionIcon"/>
  <xsl:apply-templates select="br:metadataRendering"/>
</xsl:template>

<xsl:template match="br:collectionIcon" xmlns:br="http://www.openarchives.org/OAI/2.0/branding/">
  <h3>Icon</h3>
  <xsl:choose>
    <xsl:when test="link!=''">
      <a href="{br:link}"><img src="{br:url}" alt="{br:title}" width="{br:width}" height="{br:height}" border="0" /></a>
    </xsl:when>
    <xsl:otherwise>
      <img src="{br:url}" alt="{br:title}" width="{br:width}" height="{br:height}" border="0" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="br:metadataRendering" xmlns:br="http://www.openarchives.org/OAI/2.0/branding/">
  <h3>Metadata Rendering Rule</h3>
  <table class="values">
    <tr><td class="key">URL</td>
    <td class="value"><xsl:value-of select="."/></td></tr>
    <tr><td class="key">Namespace</td>
    <td class="value"><xsl:value-of select="@metadataNamespace"/></td></tr>
    <tr><td class="key">Mime Type</td>
    <td class="value"><xsl:value-of select="@mimetype"/></td></tr>
  </table>
</xsl:template>



<!--
   Identify / Gateway
-->

<xsl:template match="gw:gateway" xmlns:gw="http://www.openarchives.org/OAI/2.0/gateway/x">
  <h2>Gateway Information</h2>
  <table class="values">
    <tr><td class="key">Source</td>
    <td class="value"><xsl:value-of select="gw:source"/></td></tr>
    <tr><td class="key">Description</td>
    <td class="value"><xsl:value-of select="gw:gatewayDescription"/></td></tr>
    <xsl:apply-templates select="gw:gatewayAdmin"/>
    <xsl:if test="gw:gatewayURL">
      <tr><td class="key">URL</td>
      <td class="value"><xsl:value-of select="gw:gatewayURL"/></td></tr>
    </xsl:if>
    <xsl:if test="gw:gatewayNotes">
      <tr><td class="key">Notes</td>
      <td class="value"><xsl:value-of select="gw:gatewayNotes"/></td></tr>
    </xsl:if>
  </table>
</xsl:template>

<xsl:template match="gw:gatewayAdmin" xmlns:gw="http://www.openarchives.org/OAI/2.0/gateway/">
  <tr><td class="key">Admin</td>
  <td class="value"><xsl:value-of select="."/></td></tr>
</xsl:template>


<!-- GetRecord -->

<xsl:template match="oai:GetRecord">
  <xsl:apply-templates select="oai:record" />
</xsl:template>

<!-- ListRecords -->

<xsl:template match="oai:ListRecords">
  <xsl:apply-templates select="oai:record" />
  <xsl:apply-templates select="oai:resumptionToken" />
</xsl:template>

<!-- ListIdentifiers -->

<xsl:template match="oai:ListIdentifiers">
  <xsl:apply-templates select="oai:header" />
  <xsl:apply-templates select="oai:resumptionToken" />
</xsl:template>

<!-- ListSets -->

<xsl:template match="oai:ListSets">
  <xsl:apply-templates select="oai:set" />
  <xsl:apply-templates select="oai:resumptionToken" />
</xsl:template>

<xsl:template match="oai:set">
  <h2>Set</h2>
  <table class="values">
    <tr><td class="key">setName</td>
    <td class="value"><xsl:value-of select="oai:setName"/></td></tr>
    <xsl:apply-templates select="oai:setSpec" />
  </table>
</xsl:template>

<!-- ListMetadataFormats -->

<xsl:template match="oai:ListMetadataFormats">
  <xsl:choose>
    <xsl:when test="$identifier">
      <p>This is a list of metadata formats available for the record "<xsl:value-of select='$identifier' />". Use these links to view the metadata: <xsl:apply-templates select="oai:metadataFormat/oai:metadataPrefix" /></p>
    </xsl:when>
    <xsl:otherwise>
      <p>This is a list of metadata formats available from this archive.</p>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates select="oai:metadataFormat" />
</xsl:template>

<xsl:template match="oai:metadataFormat">
  <h2>Metadata Format</h2>
  <table class="values">
    <tr><td class="key">metadataPrefix</td>
    <td class="value"><a class="link" href="?verb=ListRecords&amp;metadataPrefix={oai:metadataPrefix}"><xsl:value-of select="oai:metadataPrefix"/></a></td></tr>
    <tr><td class="key">metadataNamespace</td>
    <td class="value"><xsl:value-of select="oai:metadataNamespace"/></td></tr>
    <tr><td class="key">schema</td>
    <td class="value"><a href="{oai:schema}"><xsl:value-of select="oai:schema"/></a></td></tr>
  </table>
</xsl:template>

<xsl:template match="oai:metadataPrefix">
      <xsl:text> </xsl:text><a class="link" href="?verb=GetRecord&amp;metadataPrefix={.}&amp;identifier={$identifier}"><xsl:value-of select='.' /></a>
</xsl:template>

<!-- record object -->

<xsl:template match="oai:record">
  <h2 class="oaiRecordTitle">OAI Record: <xsl:value-of select="oai:header/oai:identifier"/></h2>
  <div class="oaiRecord">
    <xsl:apply-templates select="oai:header" />
    <xsl:apply-templates select="oai:metadata" />
    <xsl:apply-templates select="oai:about" />
  </div>
</xsl:template>

<xsl:template match="oai:header">
  <h3>OAI Record Header</h3>
  <xsl:variable name="metadataPrefix" select="/oai:OAI-PMH/oai:request/@metadataPrefix"/>
  <table class="values">
    <tr><td class="key">OAI Identifier</td>
    <td class="value">
      <xsl:value-of select="oai:identifier"/>
      <xsl:text> </xsl:text><a class="link" href="?verb=GetRecord&amp;metadataPrefix={$metadataPrefix}&amp;identifier={oai:identifier}"><xsl:value-of select="$metadataPrefix"/></a>
      <xsl:text> </xsl:text><a class="link" href="?verb=ListMetadataFormats&amp;identifier={oai:identifier}">formats</a>
    </td></tr>
    <tr><td class="key">Datestamp</td>
    <td class="value"><xsl:value-of select="oai:datestamp"/></td></tr>
  <xsl:apply-templates select="oai:setSpec" />
  </table>
  <xsl:if test="@status='deleted'">
    <p>This record has been deleted.</p>
  </xsl:if>
</xsl:template>


<xsl:template match="oai:about">
  <p>"about" part of record container not supported by the XSL</p>
</xsl:template>

<xsl:template match="oai:metadata">
  &#160;
  <div class="metadata">
    <xsl:apply-templates select="*" />
  </div>
</xsl:template>



<!-- oai setSpec object -->

<xsl:template match="oai:setSpec">
  <tr><td class="key">setSpec</td>
  <td class="value"><xsl:value-of select="."/>
    <xsl:variable name="metadataPrefix" select="/oai:OAI-PMH/oai:request/@metadataPrefix"/>
    <xsl:choose>
      <xsl:when test="$metadataPrefix">
        <xsl:text> </xsl:text><a class="link" href="{concat('?verb=ListIdentifiers&amp;metadataPrefix=',$metadataPrefix,'&amp;set=',.)}">Identifiers</a>
        <xsl:text> </xsl:text><a class="link" href="{concat('?verb=ListRecords&amp;metadataPrefix=',$metadataPrefix,'&amp;set=',.)}">Records</a>
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>
  </td></tr>
</xsl:template>



<!-- oai resumptionToken -->

<xsl:template match="oai:resumptionToken">
   <p>There are more results.</p>
   <table class="values">
     <tr><td class="key">resumptionToken:</td>
     <td class="value"><xsl:value-of select="."/>
<xsl:text> </xsl:text>
<a class="link" href="?verb={/oai:OAI-PMH/oai:request/@verb}&amp;resumptionToken={.}">Resume</a></td></tr>
   </table>
</xsl:template>

<!--metadata -->
<xsl:template match="oai:metadata/*" priority='-100'>
  <h3>OAI Record Metadata</h3>
  <xsl:apply-templates select="." mode='xmlMarkup' />
</xsl:template>


<!-- oai_dc record -->

<xsl:template match="oai_dc:dc"  xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" >
  <div class="dcdata">
    <h3>Dublin Core Metadata (oai_dc)</h3>
    <table class="dcdata">
      <xsl:apply-templates select="*" />
    </table>
  </div>
</xsl:template>

<xsl:template match="dc:title" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Title</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:creator" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Author or Creator</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:subject" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Subject and Keywords</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:description" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Description</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:publisher" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Publisher</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:contributor" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Other Contributor</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:date" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Date</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:type" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Resource Type</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:format" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Format</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:identifier" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Resource Identifier</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:source" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Source</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:language" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Language</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:relation" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Relation</td><td class="value">
  <xsl:choose>
    <xsl:when test='starts-with(.,"http" )'>
      <xsl:choose>
        <xsl:when test='string-length(.) &gt; 50'>
          <a class="link" href="{.}">URL</a>
          <i> URL not shown as it is very long.</i>
        </xsl:when>
        <xsl:otherwise>
          <a href="{.}"><xsl:value-of select="."/></a>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="."/>
    </xsl:otherwise>
  </xsl:choose>
</td></tr></xsl:template>

<xsl:template match="dc:coverage" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Coverage</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<xsl:template match="dc:rights" xmlns:dc="http://purl.org/dc/elements/1.1/">
<tr><td class="key">Rights Management</td><td class="value"><xsl:value-of select="."/></td></tr></xsl:template>

<!-- XML Pretty Maker -->

<xsl:template match="node()" mode='xmlMarkup'>
  <div class="xmlBlock">
    &lt;<span class="xmlTagName"><xsl:value-of select='name(.)' /></span><xsl:apply-templates select="@*" mode='xmlMarkup'/>&gt;<xsl:apply-templates select="node()" mode='xmlMarkup' />&lt;/<span class="xmlTagName"><xsl:value-of select='name(.)' /></span>&gt;
  </div>
</xsl:template>

<xsl:template match="text()" mode='xmlMarkup'><span class="xmlText"><xsl:value-of select='.' /></span></xsl:template>

<xsl:template match="@*" mode='xmlMarkup'>
  <xsl:text> </xsl:text><span class="xmlAttrName"><xsl:value-of select='name()' /></span>="<span class="xmlAttrValue"><xsl:value-of select='.' /></span>"
</xsl:template>

<xsl:template name="xmlstyle">
.xmlSource {
	font-size: 70%;
	border: solid #c0c0a0 1px;
	background-color: #ffffe0;
	padding: 2em 2em 2em 0em;
}
.xmlBlock {
	padding-left: 2em;
}
.xmlTagName {
	color: #800000;
	font-weight: bold;
}
.xmlAttrName {
	font-weight: bold;
}
.xmlAttrValue {
	color: #0000c0;
}
</xsl:template>

</xsl:stylesheet>
    """


def default_index_html():
    '''
    If no default index page found, use this embedded one to get up and running quickly.
    '''

    return """
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>OAI-PMH 2.0 Index</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" CONTENT="noindex" />
    <meta name="googlebot" content="noindex" />
  </head>
  <body>
    <h1>OAI-PMH 2.0</h1>
    <p>This page is not intended for human consumption and exists to expose OAI-PMH results to web crawlers and other automated clients.</p>
    <h2>OAI Queries</h2>
    <!-- NOTE: id on list element MUST be oai_query_list -->
    <ul id="oai_query_list">
      <!-- These queries don't require 'metadataPrefix' argument so we can hard code them.
           Other queries requiring metadataPrefix are dynamically inserted.
      -->
      <li><a href="?verb=Identify">Identify</a></li>
      <li><a href="?verb=ListMetadataFormats">ListMetadataFormats</a></li>
      <li><a href="?verb=ListSets">ListSets</a></li>
    </ul>
  </body>
</html>
    """



class oai_xhtml_wrapper:
    '''
    Wrapper for OAI WSGI application which performs XSL transform to XHTML
    if client is not accepting XML content type.
    '''

    def __init__ (self, application, config={}):
        self.__application = application
        self.__environ = None
        self.__start_response = None
        self.__config = config
        self.__oai_base_href = ''
        sys.stderr.write("DEBUG: Got config=" + str(config) + "]\n")


    def get_default_oai_xslt(self):
        '''
        Return default OAI XSLT.
        '''

        return default_xslt_body()


    def get_static_asset(self, fileName, defaultData=''):
        '''
        Return contents of a given file residing in the configured xslt_files_root (static) location
        '''

        static_path = self.__config.get('xslt_files_root', os.path.realpath(__file__))
        file_path = os.path.join(static_path, fileName)
        if os.path.isfile(file_path):
            try:
                f = open(file_path, 'r')
                data = f.read()
                f.close()
                return data
            except:
                ## swallow any exceptions
                pass
        return defaultData


    def get_default_oai_css(self):
        '''
        Return default OAI CSS.
        '''

        return self.get_static_asset('default_oai.css')


    def get_default_oai_index_html(self):
        '''
        Return default OAI index HTML page.

        Try for a file named 'oai_index.html' in configured static fies path, or,
        failing that use the embedded static default.         
        '''

        return self.get_static_asset('oai_index.html', default_index_html())


    def get_oai_metadata_prefixes(self):
        '''
        Parse upstream-provided OAI-PMH result of 'ListMetadataFormats' query to get
        the supported metadata formats.
        '''

        ## call wrapped OAI app with modified request for metadata formats
        responseXML = ''
        newEnviron = self.__environ.copy()
        newEnviron['QUERY_STRING'] = 'verb=ListMetadataFormats'
        for r in self.__application(newEnviron, self.__start_response):
            responseXML += r

        ## Remove namespace so lxml doesn't prepend them to element names
        ## (makes searching and matching on element names much simpler)
        responseXML = responseXML.replace(' xmlns=', ' xmlnamespace=')

        oaipmhTree = etree.parse(StringIO(responseXML))

        prefixes = [ e.text.strip() for e in oaipmhTree.findall('.//metadataPrefix') ]

        return prefixes


    def add_metadata_verb_links_to_html(self, htmlElementTree):
        '''
        Return OAI index html with auto-generated OAI queries based off 
        metadata formats we get from the OAI-PMH source.
        
        Of the five major verbs, the ones requiring 'metadataPrefix' are:

            ListIdentifiers
            ListRecords
        '''

        metadataPrefixes = self.get_oai_metadata_prefixes()
        newQueryAnchorElements = []
        for mp in metadataPrefixes:
            recordsAnchor = html.Element('a', href='?verb=ListRecords&metadataPrefix='+mp)
            recordsAnchor.text = 'ListRecords (' + mp + ')'
            identifiersAnchor = html.Element('a', href='?verb=ListIdentifiers&metadataPrefix='+mp)
            identifiersAnchor.text = 'ListIdentifiers (' + mp + ')'
            newQueryAnchorElements.append(recordsAnchor)
            newQueryAnchorElements.append(identifiersAnchor)

        ## Find list having id = "oai_query_list"
        ul = htmlElementTree.find(".//ul[@id='oai_query_list']")
        if ul is not None:
            ## append list item elements containing dynamically generated query URIs
            for elem in newQueryAnchorElements:
                li = etree.Element('li')
                li.append(elem)
                ul.append(li)

        return htmlElementTree


    def add_default_css_to_html(self, htmlElementTree):
        '''
        Inject inline css style into html <head> element.
        '''

        head = htmlElementTree.find(".//head")
        if head is not None:
            style = html.Element('style', type='text/css')
            style.text = "\n" + self.get_default_oai_css()
            head.append(style)
        return htmlElementTree


    def get_oai_index_html(self):
        '''
        Return OAI index html with inserted metadata-specific verb links
        and default CSS styling.
        '''

        indexhtml = self.get_default_oai_index_html()

        #sys.stderr.write("DEBUG: Got default HTML=[" + indexhtml + "]\n")

        htmlTree = html.fromstring(indexhtml)
        htmlTree = self.add_metadata_verb_links_to_html(htmlTree)
        htmlTree = self.add_default_css_to_html(htmlTree)

        return '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN" "http://www.w3.org/TR/html4/loose.dtd">' + "\n" + \
            etree.tostring(htmlTree, pretty_print=True, method="html")


    def get_result_format_override(self):
        return self.__config.get('result_format_override', '')


    def is_xhtml_override(self):
       return self.get_result_format_override().find('html') != -1


    def is_xml_override(self):
       return self.get_result_format_override().find('xml') != -1


    def get_default_client_accept_mimetype(self):
        '''
        Reduce the client's target accept type to "text/xhtml" or "text/xml"
        after considering global override and what the client provides us.
        '''

        ## In ALL cases where default is XHTML, we must return XHTML
        ## (including empty HTTP_ACCEPT)
        if self.is_xhtml_override():
            sys.stderr.write("DEBUG: Forcing text/xhtml return..\n")
            return 'text/xhtml'
        ## In ALL cases where default is XML, we must return XML
        ## (including empty HTTP_ACCEPT)
        if self.is_xml_override():
            sys.stderr.write("DEBUG: Forcing text/xml return..\n")
            return 'text/xml'

        accept_types = self.__environ.get('HTTP_ACCEPT', '')
        sys.stderr.write('DEBUG: Got client HTTP_ACCEPT=[' + accept_types + "]\n")

        ## No server-side override given, we only return XHTML if the client doesn't
        ## accept XML. In all other cases, including empty HTTP_ACCEPT, we return XML
        ## because that's what we do (OAI-PMH after all)
        mimetype = 'text/xml'
        if not len(self.get_result_format_override()):
            ## Non-empty HTTP_ACCEPT, not wildcard and no XML
            if len(accept_types) and accept_types.find('xml') == -1 and accept_types.find('*/*') == -1:
                mimetype = 'text/xhtml'
        sys.stderr.write('DEBUG: No default set, returning ' + mimetype + " format..\n")
        return mimetype


    def add_xsl_processing_instruction_to_xml(self, xmlTree):
        '''
        Add default <?xml-stylesheet> directive to XML tree
        '''

        xslt_file = self.__config.get('xslt_file', 'default_oai.xsl')
        ## We haven't been configured with a base URI for static content, so let's just
        ## use the "root" URI
        default_xslt_uri = self.__oai_base_href
        xslt_uri = self.__config.get('xslt_uri_root', default_xslt_uri)
        if xslt_uri == default_xslt_uri:
            xslt_uri += '?'
        else:
            xslt_uri += '/'
        ## Add stylesheet PI element immediately before the "root" <OAI-PMH> element
        xmlTree.getroot().addprevious(
            etree.ProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="' + xslt_uri + xslt_file + '"'))
        return xmlTree


    def replace_localhost_with_external(self, content):
        host_strings_to_replace = ['localhost', '127.0.0.1']
        if 'external_host' in self.__config:
            extern_host = self.__config.get('external_host', '')
            if len(extern_host):
                host_matches = []
                for host in host_strings_to_replace:
                    [ host_matches.append(match) for match in re.findall('https*\:\/\/'+host+'[:0-9]*\/', content) ]

                for match in host_matches:
                    if len(match):
                        if extern_host[-1] != '/':
                            extern_host += '/'
                        content = content.replace(match, extern_host)
        return content
 

    def process_oai_xml(self):
        '''
        Return OAI result with or without transform.

        Input OAI XML feed comes from the wrapped (upstream) WSGI application.

        If client is not XML capable, we return a HTML view of it otherwise we return it
        as-is with the possible exception of insertion of client-side XSL include, so 
        client browser can attempt to render the XML with a stylesheet.
        '''

        ## Get XML result from wrapped OAI application (assumes total response iterable
        ## is the complete XML result)
        oai_result = ''
        for r in self.__application(self.__environ, self.__start_response):
            oai_result += r


        ## If external host was configured, replace any http(s)://<local_host_or_IP:portspec>/
        ## with external host name so output URIs can be consumed validly.
        oai_result = self.replace_localhost_with_external(oai_result)

        oai_result_tree = etree.ElementTree(etree.fromstring(oai_result))

        ## Inject XSLT processing directive for human / HTML capable client consumption,
        ## if we didn't get any style sheet directive from the OAI layer's XML result.
        if oai_result.find("<?xml-stylesheet") == -1:
            oai_result_tree = self.add_xsl_processing_instruction_to_xml(oai_result_tree)

        ## Determine simplified XML or XHTML mimetype we need to return
        accept_types = self.get_default_client_accept_mimetype()

        ## Requester wants XHTML (or we've determimed as such) so do a server-side transform to XHTML.
        ## NOTE: Original client-side HTTP_ACCEPT has been replaced by either "text/xml" or "text/xhtml"
        ##
        ##       See: self.get_default_client_accept_mimetype()
        ###
        if accept_types.find('html') != -1:
            xslt_path = self.__config.get('xslt_files_root', os.path.realpath(__file__))
            xslt_name = self.__config.get('xslt_file', '')
            xslt_path = os.path.join(xslt_path, xslt_name)

            sys.stderr.write("DEBUG: final xslt_path=[" + xslt_path + "]\n")

            xslt_raw = self.get_static_asset(xslt_name, default_xslt_body())

            xslt = etree.XML(xslt_raw)
            transform = etree.XSLT(xslt)
            self.call_start_response('200 OK', [('Content-Type','text/html')])
            return [transform(etree.XML(oai_result))]

        ## Requester handles XML, so return without XHTML transform
        self.call_start_response('200 OK', [('Content-Type',"text/xml")])
        return ['<?xml version="1.0" encoding="UTF-8"?>' + "\n" + etree.tostring(oai_result_tree, pretty_print=True, encoding='utf-8')]


    def call_start_response(self, status, headers, exc_info=sys.exc_info()):
        '''
        Wrap start_response() so it works for WSGI implementations that expect either
        2 or 3 arguments to start_response()
        '''

        try:
            self.__start_response(status, headers, exc_info)
        ## Too many arguments, presumably - let's drop the exc_info
        except TypeError:
            self.__start_response(status, headers)


    def __call__(self, environ, start_response):
        '''
        Standard WSGI entrypoint. Process request and return response to client.
        '''

        self.__environ = environ
        self.__start_response = start_response

        query_string = self.__environ.get('QUERY_STRING')
        ## Return default index HTML page if we hit on OAI URI with no query
        ## (replaces "badVerb" return from OAI). Otherwise, pass through.
        if not len(query_string):
            ## Where are we now? Set current OAI URI so we can build more URIs from it
            self.__oai_base_href = self.__environ.get('wsgi.url_scheme') + '://' + self.__environ.get('HTTP_HOST') + self.__environ.get('SCRIPT_NAME')
            ## Now, return an index page because no OAI query was given
            index_html = self.get_oai_index_html()
            if len(index_html):
                self.call_start_response('200 OK', [('Content-Type','text/html'), ('X-Robots-Tag','noindex')])
                return [index_html]

        ## If query was for default OAI XSL, return it
        elif query_string.find('default_oai.xsl') != -1:
            self.call_start_response('200 OK', [('Content-Type','text/xml')])
            return [self.get_default_oai_xslt()]

        ## Return OAI result with or without transform
        return self.process_oai_xml()
