from oai_xhtml_wrapper import oai_xhtml_wrapper


def oai_filter(app, context, **settings):
    '''
    Paste FilterFactory boilerplate
    '''

    return oai_xhtml_wrapper(app, settings)
